from rest_framework import routers, serializers, viewsets
from rest_framework.response import Response
from rest_framework.status import *
from .models import *



class MuffinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Muffin


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order


class OrderMuffinSerializer(OrderSerializer):
    muffin = MuffinSerializer()


class MuffinViewSet(viewsets.ModelViewSet):
    queryset = Muffin.objects.all()
    serializer_class = MuffinSerializer


class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer

    @property
    def muffin(self):
        return self.request.GET.get('muffin')

    def get_queryset(self):
        orders = Order.objects.all()
        if self.muffin:
            orders = Order.objects.filter(muffin__id=self.muffin)
        return orders

    def list(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = OrderMuffinSerializer(queryset, many=True)
        return Response(serializer.data)
