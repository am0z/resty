from django.db import models


class Muffin(models.Model):
    name = models.CharField(max_length=255)
    size = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class Order(models.Model):
    muffin = models.ForeignKey(Muffin)
    amount = models.PositiveSmallIntegerField()
    urgency = models.PositiveSmallIntegerField(
        choices=((0, 'yes, please'), (1, 'meh'), (2, 'come on')),
        default=2
    )
