from django.conf.urls import patterns, include, url
from rest_framework import routers, serializers, viewsets
from .models import *
from .views import *
from .rest import *


router = routers.DefaultRouter()
router.register(r'muffins', MuffinViewSet)
router.register(r'orders', OrderViewSet, base_name='order')


urlpatterns = patterns('',
    url(r'^$', OrderListView.as_view()),
    url(r'^', include(router.urls)),
)
