from django.shortcuts import render
from django.views.generic import ListView
from .models import *


class OrderListView(ListView):
    model = Order

    @property
    def query(self):
        return self.request.GET.get('query', '')

    def get_queryset(self):
        orders = super(OrderListView, self).get_queryset()
        try:
            orders = orders.filter(muffin__id=self.query)
        except ValueError:
            orders = orders.filter(muffin__name__icontains=self.query)
        return orders

    def get_context_data(self, **kwargs):
        context = super(OrderListView, self).get_context_data(**kwargs)
        context.update({
            'query': self.query
        })
        return context
