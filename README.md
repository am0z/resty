# howto #

Resty app to order muffins via API.

### Models ###

* Muffin
* Order

### List ###

To get list of muffins:

```
#!shell

GET /muffins/
```
Example output:

```
#!json

[
    {
        "id": 1,
        "name": "bestest",
        "size": "huge"
    },
    {
        "id": 2,
        "name": "my favourite",
        "size": "tiny"
    }
]
```


To get list of orders with related muffin inlined:

```
#!shell

GET /order/
```
Example output:

```
#!json

[
    {
        "amount": 5,
        "id": 1,
        "muffin": {
            "id": 1,
            "name": "bestest",
            "size": "huge"
        },
        "urgency": 0
    },
    {
        "amount": 1,
        "id": 2,
        "muffin": {
            "id": 2,
            "name": "my favourite",
            "size": "tiny"
        },
        "urgency": 2
    },
    {
        "amount": 12,
        "id": 3,
        "muffin": {
            "id": 2,
            "name": "my favourite",
            "size": "tiny"
        },
        "urgency": 2
    }
]
```

### Creating new muffin ###

```
#!shell


POST /muffins/
```
data

```
#!json

{"name": "awesome muffin", "size": "itty-bitty"}
```
Sample output

```
#!json

{"id":3,"name":"awesome muffin","size":"itty-bitty"}
```


### Creating new order ###

```
#!shell


POST /orders/
```
data

```
#!json

{"muffin": 3, "amount": 10, "urgency": 2}
```
Sample output

```
#!json

{"id":3,"name":"awesome muffin","size":"itty-bitty", "muffin": 3}
```


### Updating muffin ###

```
#!shell


PUT /muffins/3/
```
data

```
#!json

{"name": "even better muffin", "size": "average"}
```
Sample output

```
#!json

{"id":3,"name":"even more better muffin","size":"average"}
```

### Deleting muffin ###

```
#!shell

DELETE /muffins/4/
```

Updatin and deleting orders is the same as for muffins.